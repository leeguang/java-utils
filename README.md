# java-utils

#### 介绍
java中用到的工具包性质的代码，轻度依赖其他框架

#### 软件架构
软件架构说明

1. javacsv：读写csv文件工具类
    - CsvReader
    - CsvWriter

从[https://sourceforge.net/projects/javacsv/](https://sourceforge.net/projects/javacsv/)搬运而来，初始版本v2.1。
具体说明见[博客园](https://www.cnblogs.com/lee2guang/p/12569248.html)。


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
